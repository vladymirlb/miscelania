#ifndef _NODE_H
#define _NODE_H

#include <math.h>

typedef struct node {
    int key;
    void* value;
    struct node** forward;
    int level;
} Node;

Node* createNode(int key, void* value, int maxLevel);

/*Node* createHeader(int maxLevel);
Node* createNIL(int maxLevel);*/

void initializeForward(Node* node, int maxLevel);
void deleteNode(Node* node);
#endif
