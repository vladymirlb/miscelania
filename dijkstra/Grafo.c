/*
 * Grafo.c
 *
 *	Aqui estao as implementacoes dos algoritmo de Dijkstra para calcular o menor caminho unico
 *	entre um vertice e os demais, e os metodos adicionais.
 *
 *  Created on: 10/12/2008
 *      Author: Vladymir Bezerra
 */

#include "Grafo.h"

/*
 * Este metodo retorna a distancia entre 2 vertices.
 * PARAM: Grafo - grafo
 * PARAM: Vertice origem - origem
 * PARAM: Vertice destino - destino
 * RETURN: int - a distancia entre os dois vertices.
 */
int getDistancia(Grafo *grafo, Vertice *origem, Vertice *destino){
	return grafo->matriz[origem->registro-1][destino->registro-1];
}
/*
 * Este metodo relaxa a distancia entre dois vertices. Se a distancia do metodo destino
 * for maior que a soma entre a distancia do vertice origem mais a distancia entre os dois
 * vertices, entao relaxe o vertice destino.
 */
void relax(Grafo *grafo, Vertice *origem, Vertice *destino){
	int distancia = origem->distancia_final+getDistancia(grafo,origem,destino);
	if(destino->distancia_final == -1 || destino->distancia_final > distancia){
		destino->distancia_final = distancia;
	}
}

/*
 * Algoritmo de Dijkstra, dado um grafo G (dirigido e ponderado) comece pela origem e visite todos seus adjacentes
 * relaxando as distancias. E assim por diante, ate percorrer todos os vertices.
 */
void Dijkstra(Grafo *grafo, Vertice **vertices, int numero_de_vertices){
	int i, count=0;
	vertices[0]->distancia_final = 0;

	Vertice *u=(Vertice*)malloc(sizeof(Vertice));

	while(count<numero_de_vertices){

		u=(Vertice*)vertices[count];
		for(i=0;i<numero_de_vertices;i++){
			if(getDistancia(grafo,grafo->vertices[count],grafo->vertices[i])==0)
				continue;
			relax(grafo,grafo->vertices[count],grafo->vertices[i]);
		}
		count++;
	}

	for(i=1;i<numero_de_vertices;i++){
		printf("Distancia do vertice 1 ao %d = %d\n",vertices[i]->registro,vertices[i]->distancia_final);
	}
}
