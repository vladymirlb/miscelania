#include <stdio.h>
#include "skiplist.c"
#include "node.h"

void main(void){
    SkipList* skiplist = 0;
    skiplist = createSkipList(10);    
    int i = 0;
    for(; i < 200 ; i++) {
         insert(skiplist, i, (int*)i);
    }
    traverse(skiplist);
    printf("%d\n", find(173, skiplist->head));
    releaseSkipList(skiplist);
}
