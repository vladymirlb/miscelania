#ifndef _SKIPLIST_H_
#define _SKIPLIST_H_

#include <sys/time.h>
#include "node.h"
#include "util.h"

typedef struct skiplist {
    Node* head;
    Node* tail;
    int level;
    int maxLevel;
    float probability;
} SkipList;

SkipList* createSkipList(int maxLevel);
void initialize_forward_nodes(SkipList* skiplist);
void releaseSkipList(SkipList* skiplist);
Node* destroyNode(Node* node, int maxLevel);

bool find(int value, const Node* head);
Node* createHead(int maxLevel);
Node* createTail(int maxLevel);
int generateNodeLevel(double p, int maxLevel);
void insert(SkipList* list, int key, void* value);
void traverse(SkipList* list);
#endif
