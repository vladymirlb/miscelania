/*
 * Grafo.h
 *
 * Arquivo de cabe�alho que define um grafo.
 *
 *  Created on: 23/12/2008
 *      Author: Vladymir Bezerra
 */
#include "Vertice.h"
#include <stdlib.h>
#include <stdio.h>

/**
 * Definicao do grafo
 */
struct meu_grafo {
	Vertice **vertices;
	int **matriz;
	int numero_de_vertices;
};

/**
 * Criando novo tipo "Grafo"
 */
typedef struct meu_grafo Grafo;

/**
 * Prototipos das funcoes.
 */
int getDistancia(Grafo *grafo,Vertice *origem, Vertice *destino);
void Dijkstra(Grafo *grafo, Vertice **vertices, int numero_de_vertices);
void relax(Grafo *grafo, Vertice *origem, Vertice *destino);
void init_grafo(Grafo *grafo);
