#ifndef _UTIL_H_
#define _UTIL_H_

enum boolean {
	False = 0,
	True = 1,
};

typedef enum boolean bool;
#endif