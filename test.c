#include <stdio.h>
#include <math.h>
#include <stdlib.h>

double test(double x);

int main(int argc, char **argv) {
	printf("%f\n", test(atof(argv[1])));
	return 0;
}

double test(double x) {
	return ( pow(x,3) + 3*(pow(x,2)) - 6*x - 8);
}
