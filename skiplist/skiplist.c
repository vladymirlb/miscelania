#include <stdlib.h>
#include "skiplist.h"
#include "node.c"
#include <time.h>

SkipList* createSkipList(int maxLevel) {
    SkipList* skiplist = (SkipList*)malloc(sizeof(SkipList));
    skiplist->maxLevel = maxLevel;
    skiplist->level = 1;
    skiplist->probability = 0.25;
    skiplist->head = createHead(maxLevel);
    skiplist->tail = createTail(maxLevel);
    skiplist->head->level = maxLevel;
    skiplist->tail->level = maxLevel;
    /*skiplist->head = createHeader(maxLevel);
    skiplist->NIL = createNIL(maxLevel);*/

    initialize_forward_nodes(skiplist);
    return skiplist;
}

Node* createHead(int maxLevel) {
    int i = 0;      
    return createNode(-1, (void*)-1, maxLevel);
}

Node* createTail(int maxLevel) {
    double key = pow(2, maxLevel);
    return createNode((int)key, (void*)-1, maxLevel);
}
void initialize_forward_nodes(SkipList* skiplist) {
    int i;
    for(i=0; i< skiplist->maxLevel; i++){
        skiplist->head->forward[i] = skiplist->tail;
        skiplist->tail->forward[i] = 0;
    }
}

void releaseSkipList(SkipList* skiplist) {
    Node* temp = skiplist->head;
    while(temp != NULL) {
        temp = destroyNode(temp, skiplist->level);
    }
    free(skiplist);
}

Node* destroyNode(Node* node, int maxLevel){
    Node* temp = node->forward[0];
    deleteNode(node);
    return temp;
}

int generateNodeLevel(double p, int maxLevel) {
    
    int level = 1;
    struct timeval t1;
    gettimeofday(&t1, NULL);
    srand((unsigned)t1.tv_usec * t1.tv_sec);
    double random = (double)rand()/(double)RAND_MAX;

    while( random < p ) {
        level++;
        gettimeofday(&t1, NULL);
        srand((unsigned)t1.tv_usec * t1.tv_sec);
        random = (double)rand()/(double)RAND_MAX;
    }
    return (level > maxLevel) ? maxLevel : level;
}

bool find(int key, const Node* head) {
    int level = head->level-1;
    Node* node = head;
    for(; level>=0 ; level--){
        if( node->forward[level]->forward[level] == NULL) continue;
        else
	while( node->forward[level]->key < key ) {
            node = node->forward[level];
        }
    }
    if( node->forward[0]->key == key )
        return True;
    else return False;
}
void traverse(SkipList *list) {
    Node* head = list->head;
    while(head->forward[0] != NULL) {
        printf("Node value = %d\n",head->key);
	printf("Node level = %d\n",head->level);
        head = head->forward[0];  
    }  
}
void insert(SkipList* list, int key, void* value) {
    Node* update[list->maxLevel];
    Node* x = list->head;
    int i = list->level-1;
    for(; i>=0; i--){
        while( x->forward[i]->key < key || x->forward[i]->forward[i] != NULL) {
             x = x->forward[i];
        }
        update[i] = x;
    }
    x = x->forward[0];
    if (x->key == key) {
        x->value = value;
	return;
    }
    else {
        int level = generateNodeLevel(list->probability, list->maxLevel);
        if(level > list->level) {
            for(i=list->level+1; i<=level; i++)
                update[i-1] = list->head;
            list->level = level;
        }
        printf("Level of the node been created: %d\n",level);
        x = createNode(key, value, level);
        for(i=0 ; i< level; i++) {
            x->forward[i] = update[i]->forward[i];
            update[i]->forward[i] = x;
        }
    }
}
