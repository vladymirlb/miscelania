#include <boos/asio.hpp>

namespace asio = boost::asio;

class Server
{
    public:
        explicit Server();
        ~Server();

    private:
        asio::io_service io_service;
        asio::ip::tcp::socket socket;
