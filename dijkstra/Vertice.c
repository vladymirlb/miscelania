/*
 * Vertice.c
 *
 *  Created on: 09/12/2008
 *      Author: Vladymir Bezerra
 */
#include <stdio.h>
#include <stdlib.h>
#include "Vertice.h"


/*
 * Este metodo inicializa uma lista encadeada vazia.
 * PARAM: Lista - uma lista encadeada.
 * RETURN: void
 */
void init_lista(Lista *lista){
	lista->cabeca = NULL;
	lista->tamanho = 0;
}
/*
 * Este metodo inicializa um vertice.
 * PARAM: Vertice - um vertice
 * PARAM: int - o valor do vertice
 */
void init_vertice(Vertice *vertice, int i){
	vertice->distancia_final= -1;
	vertice->distancia_predecessor=-1;
	vertice->next=NULL;
	vertice->registro=i;
}
/*
 * Este metodo insere ordenadamente em uma lista encadeada.
 * PARAM: Lista - uma lista encadeada
 * PARAM: Vertice - vertice a ser adicionado.
 */
void insere_ordenado(Lista *lista, Vertice *vertice){
	Vertice *aux = lista->cabeca;
	if(lista->cabeca==NULL){
		lista->cabeca=vertice;
	}
	else if(vertice->registro < aux->registro){
		vertice->next = aux;
		lista->cabeca = vertice;
	}
	else {
		while(aux->next != NULL && vertice->registro > aux->next->registro)
			aux = aux->next;
		vertice->next = aux->next;
		aux->next = vertice;
		return;
	}
	lista->tamanho++;
}

/*
 * Este metodo extrai o minimo em uma lista de prioridades.
 * PARAM: Lista - lista que vai ser removida
 * RETURN: Vertice - vertice removido.
 */
Vertice *extract_min(Lista *lista){
	Vertice *minimo;
	if(lista->cabeca == NULL)
		return NULL;
	else {
		minimo = lista->cabeca;
		lista->cabeca = lista->cabeca->next;
		return minimo;
	}
}


