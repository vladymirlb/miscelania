/*
 * Vertice.h
 *
 *	Arquivo de cabe�alho que representa um vertice do grafo.
 *
 *  Created on: 10/12/2008
 *      Author: Vladymir Bezerra
 */

struct lista_ordenada {
	struct meu_vertice *cabeca;
	int tamanho;
};

struct meu_vertice {
	int registro;
	int distancia_final;
	int distancia_predecessor;
	struct meu_vertice *next;
};


typedef struct meu_vertice Vertice;
typedef struct lista_ordenada Lista;



void percorre(Lista *lista);
void insere_ordenado(Lista *lista, Vertice *vertice);
void init_lista(Lista *lista);
void init_vertice(Vertice *vertice, int i);
Vertice *extract_min(Lista *lista);
