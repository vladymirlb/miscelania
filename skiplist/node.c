#include <stdlib.h>
#include "node.h"

Node* createNode(int key, void* value, int maxLevel) {
    Node* node =(Node*) malloc(sizeof(Node));
    node->key = key;
    node->value = (void*)value;
    initializeForward(node, maxLevel);
    node->level = maxLevel;
    return node;
}

/*Node* createHead(int maxLevel) {

    Node* head = (Node*)malloc(sizeof(Node));/*
    head->key = -1;
    head->value = (void*)-1;
    initializeForward(head, maxLevel);
    head = createNode(-1, -1, maxLevel);
    return head;
}

Node* createTail(int maxLevel){
    Node* Tail = (Node*)malloc(sizeof(Node));
    /*Tail->key = pow(2,maxLevel+1);
    Tail->value = (void*)-1;
    initializeForward(Tail, maxLevel);
    Tail = createNode( ((int)(pow(2, maxLevel+1))), (void*)-1, maxLevel);
    return Tail;*
}*/
void deleteNode(Node* node) {
    if(node->forward) 
        free(node->forward);
    free(node);
}

void initializeForward(Node* node, int maxLevel){
    int i;
    node->forward = (Node**)malloc(sizeof(Node)*maxLevel);
    if(!node) return;
    for(i=0; i<maxLevel; i++)
        node->forward[i] = 0;
}
