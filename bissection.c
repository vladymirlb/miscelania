/**
 *  Exercicio de MSN
 *  Professor: Antao Moura
 *  Aluno: Vladymir Bezerra
 *  Metodo de resolucao: Bisseccao
 *  Funcao: f(x) = 3x^2 -x + ln(x)
 *  compilar com: gcc bissection.c -o bis -lm
 */

#include <stdio.h>
#include <math.h>
#include <stdlib.h>

/**
 * Funcao que encontra raizes de uma equacao nao-linear a partir
 * do metodo da Bisseccao.
 * Parametros: ival1, ival2: intervalo
 *             tolerancia: nivel minimo de tolerancia
 *             iter: numero de iteracoes a serem executadas
 *             (*functor)(double): ponteiro para a funcao que desejamos resolver
 */
void bisseccao(double ival1, double ival2, double tolerancia, int iter,
        double (*functor)(double));

double funcao(double x);

int main(int argc, char **argv) {
    bisseccao(-20,15,0.000002,100,funcao);
    return 0;

}

void bisseccao(double ival1, double ival2, double tolerancia, int iter,
        double (*functor)(double)) {
    int k = 0;
    double x0 = ival1, x1=ival2;
    double media =(double) (x0+x1)/2.0;
    double parametro = (*functor)(media);
    if(parametro < 0)
        parametro *= -1;
    
    printf("k\ta\t\tb\t\tf(a)\t\tf(b)\t\traiz\t\tf(raiz)\n" );
    printf("%d\t%f\t%f\t%f\t%f\t%f\t%f\n", k,x0,x1,functor(x0),functor(x1),media,functor(media));
    while(parametro >  tolerancia && k <= iter) {
        if(((*functor)(x0) * (*functor)(media)) < 0) {
            x1 = media;
        } else {
            x0 = media;
        }
        media = (double)(x0+x1)/2.0;
        k++;
        parametro = (*functor)(media);
        if(parametro < 0)
            parametro *= -1;
	printf("%d\t%f\t%f\t%f\t%f\t%f\t%f\n", k,x0,x1,functor(x0),functor(x1),media,functor(media));        
    }
    if (k > iter) {
        printf("Nao converge\n");
        return;
    }
    printf("Convergiu: raiz = %f\n", media);
    printf("Resultado: %f\n", (*functor)(media));
}

double funcao(double x) {
	return ( pow(x,3) + 3*(pow(x,2)) - 6*x - 8);
}
