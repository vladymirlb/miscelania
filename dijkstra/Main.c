/*
 * Main.c
 *
 *	Este programa cria um grafo e aplica o algoritmo de Dijkstra para calcular a menor distancia
 *	entre um vertice e os demais.
 *
 *  Created on: 23/12/2008
 *      Author: Vladymir Bezerra
 */
#include <stdio.h>
#include "Grafo.h"
#include <stdlib.h>


int main(int argc, char **argv) {
	int numero_de_vertices;

	Grafo *grafo;
	int i,j;

	if(argc < 2){
		printf("Digite %s numero_de_vertices\n",argv[0]);
		exit(0);
	}

	//transforma a entrada em inteiro
	numero_de_vertices = atoi(argv[1]);

	/*
	 * Inicializando os atributos
	 */

	grafo=(Grafo*)malloc(sizeof(Grafo));

	grafo->vertices=(Vertice**)malloc(sizeof(Vertice)*numero_de_vertices);

	grafo->matriz=(int**)malloc(sizeof(int)*numero_de_vertices);

	for(i=0;i<numero_de_vertices;i++){
			grafo->matriz[i]=(int*)malloc(sizeof(int));
			grafo->vertices[i]=(Vertice*)malloc(sizeof(Vertice));
	}

	for(i=0;i<numero_de_vertices;i++){
		init_vertice(grafo->vertices[i],i+1);
		grafo->vertices[i]->registro=i+1;
	}

/*
 * Zerando a matriz de distancias.
 */
	for(i=0;i<numero_de_vertices;i++){
		for(j=0;j<numero_de_vertices;j++)
				grafo->matriz[i][j]=0;
	}
	/*
	 * Recebe as distancias entre os vertices.
	 */
	for(i=0;i<numero_de_vertices;i++)
		for(j=0;j<numero_de_vertices;j++){
			if(i==j) {
				grafo->matriz[i][j]=0;   //distancia de 1 para 1, 2 para 2, etc � sempre 0
				continue;
			}
			printf("Qual a distancia do vertice %d para o %d? (0 se nao houver liga�ao)\n",i+1,j+1);
			scanf("%d",&grafo->matriz[i][j]);
		}


	Dijkstra(grafo,grafo->vertices,numero_de_vertices);


	free(grafo->matriz);
	free(grafo->vertices);
	free(grafo);
	return 0;
}
